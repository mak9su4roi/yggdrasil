# Exercise VI

## Description

- Connect button from GL Raspberry Kit to Raspberry Pi Zero W using GPIO26 output
- Write bash-script for counting and displaying number of times the button is pushed
- Eliminate contact bouncing if present

## Extra

- Connect RGB moudle from GL Raspberry Kit to Raspberry Pi Zero W using GPIO16, GPIO20, GPIO21 outputs
- Write bash-script for RGB module manipulation:
	- All LEDs are on when the program has just started and is waiting for the first button to be pushed
	- All LEDs turn ON and OFF sequentially with interval of 0.5(s):
	`R -> G -> B -> R ...`
	- Sequence of LEDs turning ON and OFF changes to reverse one after button is pushed
	- Eliminate contact bouncing if present
