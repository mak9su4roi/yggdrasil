#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include "../inc/lcd_display.h"

#define SINGLEDEVICE 0
#define DEVICENUM 1
#define MODNAME "lcd_display_mod"
#define DEVNAME "display"
#define DEVCLASS "rpi_lcd"

enum lcd_commands lcd_command = NONE;

static int major;
static ssize_t data_write(struct file *file, const char *buf, size_t count,
			  loff_t *ppos);
static struct cdev hcdev;
static struct class *devclass;
static const struct file_operations dev_fops = {
	.owner = THIS_MODULE,
	.write = data_write,
};

static ssize_t data_write(struct file *file, const char *buf, size_t count,
			  loff_t *ppos)
{
	int res;
	u16 x0, y0, w, h, color;
	char command_buff[100] = { '\0' };
	copy_from_user(command_buff, buf, count);

	switch (lcd_command) {
	case DRAW_RECTANGLE: {
		res = sscanf(buf, "%hu|%hu|%hu|%hu|%hu", &x0, &y0, &w, &h,
			     &color);
		if (res == 5)
			lcd_fill_rectangle(x0, y0, w, h, color);
		break;
	}
	case DRAW_BALL: {
		res = sscanf(buf, "%hu|%hu|%hu", &x0, &y0, &color);
		if (res == 3) {
			lcd_put_char(x0, y0, ' ', Ball_16x16, color,
				     COLOR_BLACK);
		}
		break;
	}

	case SET_WINDOW: {
		res = sscanf(buf, "%hu|%hu|%hu|%hu", &x0, &y0, &w, &h);
		if (res == 4)
			lcd_set_address_window(x0, y0, w, h);
		break;
	}

	case RENDER: {
		res = sscanf(buf, "%hu", &x0);
		if (res == 1) {
			if (x0 == 0)
				lcd_full_update();
			else
				lcd_fast_update();
		}
		break;
	}
	default:
		pr_info("Wrong command  %d\n", lcd_command);
	}
	return count;
}

int dev_init(void)
{
	int ret;
	dev_t dev;
	ret = alloc_chrdev_region(&dev, SINGLEDEVICE, DEVICENUM, MODNAME);
	major = MAJOR(dev);
	if (ret < 0) {
		printk(KERN_ERR "Failed to register char device region\n");
		goto err;
	}
	cdev_init(&hcdev, &dev_fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICENUM);
	if (ret < 0) {
		unregister_chrdev_region(MKDEV(major, SINGLEDEVICE), DEVICENUM);
		printk(KERN_ERR "=== Can not add char device\n");
		goto err;
	}
	devclass = class_create(THIS_MODULE, DEVCLASS);
	dev = MKDEV(major, SINGLEDEVICE);
	device_create(devclass, NULL, dev, NULL, "%s", DEVNAME);
	printk(KERN_INFO "======== device installed %d:[%d-%d] ===========\n",
	       MAJOR(dev), SINGLEDEVICE, MINOR(dev));
err:
	return ret;
}

void dev_exit(void)
{
	dev_t dev;
	dev = MKDEV(major, SINGLEDEVICE);
	device_destroy(devclass, dev);
	class_destroy(devclass);
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, SINGLEDEVICE), DEVICENUM);
	printk(KERN_INFO "=============== module removed ==================\n");
}
