#ifndef LCD_DISPLAY_H
#define LCD_DISPLAY_H

//>>
#include "user_api.h"
#include "fonts.h"

typedef u16 pixel_t;

struct window {
	u16 x0;
	u16 x1;
	u16 y0;
	u16 y1;
	u16 w;
	u16 h;
};

struct frame {
	size_t size;
	u16 n_rows;
	u16 n_cols;
	pixel_t display[LCD_WIDTH * LCD_HEIGHT];
	pixel_t unraveled[LCD_WIDTH * LCD_HEIGHT];
};

extern enum lcd_commands lcd_command;

void lcd_fast_update(void);
void lcd_full_update(void);
void lcd_fill_screen(u16 color);
void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color);
void lcd_put_str(u16 x, u16 y, const char *str, FontDef font, u16 color,
		 u16 bgcolor);
void lcd_set_address_window(u16 x0, u16 y0, u16 w, u16 h);
void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor);

int display_init(void);
void display_exit(void);
//>>

#endif //LCD_DISPLAY_H
