# Author

- Maksym Bilyk
- Lviv

# Title

Arkanoid

# Description

1. Develop Character Device Driver for LCD 3.2 as Linux LKM and userspace application (Arkanoid) to demonstrate work of module.
2. RPi Zero W, RPi 3.2 LCD display

## References
1.[video](https://www.dropbox.com/s/lsygy2u0x6sazun/Arkanoid.mov?dl=0)

