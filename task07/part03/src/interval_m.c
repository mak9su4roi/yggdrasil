#include <linux/string.h>
#include <linux/device/class.h>
#include <linux/jiffies.h>
#include <linux/timekeeping.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Maksym Bilyk");
MODULE_DESCRIPTION("Example for procfs read/write");
MODULE_VERSION("0.1");

#define MODULE_TAG     "task_07"
#define BUFFER_SIZE    100

static u32 last_read;
static u32 cur_read;
char read_mod[BUFFER_SIZE];

static u32 mod=0;

static void tmmmm(void) {
	ktime_t seconds;
	seconds=ktime_get_real()/1000000000 + 3600*3;
	printk("%llu\n",(seconds/3600)%24);
}


static ssize_t time_point_show(struct class *class, struct class_attribute *attr,
		char *buf)
{
	ktime_t g_seconds, g_minutes, g_hours;
	u32 interval, seconds, minutes, hours;
	cur_read = jiffies; 
	interval = (cur_read-last_read)/HZ;
	
	if (mod==1) {
		minutes = interval/60;
		hours = minutes/60;
		minutes %= 60;
		seconds = interval%60;
		sprintf(buf, "Ful: %u:%u:%u", hours, minutes, seconds);
	} else if (mod==2) { 
		g_seconds=ktime_get_real()/1000000000 + 3600*3;
		g_minutes = g_seconds/60;
		g_hours = g_minutes/60;
		g_hours %= 24;
		g_minutes %= 60;
		g_seconds %= 60;
		sprintf(buf, "Abs: %llu:%llu:%llu", g_hours, g_minutes, g_seconds);
	} else {
		sprintf(buf, "Int: %u", interval);
	}
	last_read=cur_read;
	tmmmm();
	return strlen(buf);
}

static ssize_t time_point_store( struct class *class, struct class_attribute *attr,
		const char *buf, size_t count )
{
	switch (buf[0]) {
	case 'F': mod=1; break;
	case 's': mod=0; break;
	case 'A': mod=2; break;
	default: break;
	} 
	return count;
}

CLASS_ATTR_RW( time_point );
static struct class *time_point_class;

static int __init example_init(void)
{
	int err;
	time_point_class = class_create(THIS_MODULE, MODULE_TAG);
	if (IS_ERR(time_point_class)) printk("bad class create\n");
	err = class_create_file(time_point_class, &class_attr_time_point);
	printk(KERN_NOTICE MODULE_TAG "loaded\n");
	last_read = jiffies;
    	return 0;
}

static void __exit example_exit(void)
{
    class_remove_file(time_point_class, &class_attr_time_point);
    class_destroy(time_point_class);
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(example_init);
module_exit(example_exit);
