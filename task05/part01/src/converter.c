#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include "../inc/converter.h"

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Maksym Bilyk");
MODULE_DESCRIPTION("Curency converter");
MODULE_VERSION("1.0");

struct currency currencies[] = {{.token='H',.value=1*STORY_POINT}, {.token='E',.value=32*STORY_POINT}};
size_t n_currencies = sizeof(currencies)/sizeof(struct currency);

unsigned int converter_value = 0;
char *convert_from = "X";
char *convert_to = "X";

module_param(converter_value, uint, S_IRUGO);
module_param(convert_from, charp, S_IRUGO);
module_param(convert_to, charp, S_IRUGO);

static int __init converter_init(void)
{
	size_t res;
	if ((res=convert(converter_value, *convert_from, *convert_to)) == (size_t)-1) goto err;
	printk(KERN_INFO "Convert %u %c to %c = %zu", converter_value, *convert_from, *convert_to, res);
	printk(KERN_INFO "Dummy");
	return 0;
err:
	printk(KERN_INFO "ERROR converting %u %c to %c", converter_value, *convert_from, *convert_to);
	printk(KERN_INFO "Dummy");
	return 0;
}

static void __exit converter_exit(void){}

module_init(converter_init);
module_exit(converter_exit)

