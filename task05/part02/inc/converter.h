#ifndef __MY_CONVERTER_H
#define __MY_CONVERTER_H
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

#define TAG_LENGTH 3
#define STORY_POINT 1
#define BUFF_SIZE 100 
#define PROC_DIRECTORY  "converter"
#define PROC_FILENAME   "value"

enum time_stamp {
	VALID,
	INVALID
};

struct currency {
	char token[TAG_LENGTH];
	size_t value;
};


extern struct class *converter_class;
extern enum time_stamp value_stamp;
extern char convert_buf[BUFF_SIZE];
extern struct proc_dir_entry *proc_dir;
extern struct proc_dir_entry *proc_file;
extern struct currency currencies[];
extern size_t n_currencies;
extern struct proc_ops proc_fops;
extern unsigned int to_convert;
extern unsigned int converted;
extern char convert_from[TAG_LENGTH];
extern char convert_to[TAG_LENGTH];

int init_converter_pfs(void);
void cleanup_converter_pfs(void);
int init_converter_sfs(void);
void cleanup_converter_sfs(void);
#endif // __MY_CONVERTER_H
