Solve 1st subtask:
git config --global user.name "Name Surname"
git config --global user.email "mailbox@example.com"
git config --global core.autocrlf input
git config --global core.safecrlf true

---
Solve 2nd subtask:
mkdir exercise03
cd exercise03
git init

---
Solve 3rd subtask:
mkdir pro
cd pro
touch readme.md
git add .
git commit -m "repo: initial commit"

---
Solve 4th subtask:
git checkout -b first_branch
gedit readme.md 
git status
git commit -am "readme: add the command log of the 1st subtask"

---
Solve 5th subtask:
git checkout master
gedit readme.md
git commit -am "readme: add command log to solve 2nd subtask"
git log --oneline --decorate --graph --all

---
Solve 6th subtask:
git status
git merge first_branch 
git mergetool
git status
git log --oneline --decorate --graph --all

---
Solve 7th subtask:
gedit readme.md
git commit -am "readme: add command log to solve 3rd subtask"
gedit readme.md
git commit -am "readme: add command log to solve 4th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 5th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 6th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 7th subtask"
git log --oneline --decorate --graph --all

---
