# Exercise II

## Description

Write bash-script that adds `~` symbol to the name of files that haven't changed at least 1 month. If file `my_file.txt` haven't changed for 30 days then rename it to `~my_file.txt`.

## Requirements 

Directory name: `exercise02`

## Extra

Write bash-script that launches script from `Exercise I` if at least on file-name has changed in specified directory

## Notes

launcher.sh (from extra) takes either one argument (dir) or none (cur dir).