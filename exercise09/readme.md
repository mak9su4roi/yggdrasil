# Exercise 9. Virtual file system. Programming interface. Resources.

## Description

Build a simple kernel module.

## Guidance

1. Install the *Linux Kernel* headers.
```bash
sudo dnf install kernel-devel
```
2. Subtask 1. Build and test **ex01** module:

- ex01.c:
```c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int __init ex01_init(void)
{
    printk(KERN_INFO "Hello!!!\n");
    return 0;
}

static void __exit ex01_exit(void)
{
    printk(KERN_INFO "Bye...\n");
}

module_init(ex01_init);
module_exit(ex01_exit);
```
- Makefile:
```makefile
obj-m += ex01.o
all:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
    sudo dmesg -C
    sudo insmod ex01.ko
    sudo rmmod ex01.ko
    dmesg
```
3. Subtask 2. Fix bugs, build and test modules **ex02** and **test_ex02**:
- ex02.c:
```c
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("A simple example Linux module.");

static char retpref[] = "this string returned from ";

char *test_01(void)
{
    static char res[80];
    strcpy(res, retpref);
    strcat(res, __FUNCTION__);
    return res;
}
EXPORT_SYMBOL(test_01);

char *test_02(void)
{
    static char res[80];
    strcpy(res, retpref);
    strcat(res, __FUNCTION__);
    return res;
}
EXPORT_SYMBOL(test_02);

static int __init xinit(void)
{
    printk(KERN_INFO "ex02: Hello!!!\n");
    printk(KERN_INFO "ex02: %s\n", test_01());
    printk(KERN_INFO "ex02: %s\n", test_02());
    return 0;
}

static void __exit xexit(void)
{
    printk(KERN_INFO "ex02: Bye...\n");
}
```
- test_ex02.c
```c
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_VERSION("0.01");
MODULE_DESCRIPTION("Test ex02 Linux module.");

extern char *test_01(void);
extern char *test_02(void);

static int __init xinit(void)
{
    printk(KERN_INFO "test_ex02: Hello!!!\n");
    printk(KERN_INFO "test_ex02: %s\n", test_01());
    printk(KERN_INFO "test_ex02: %s\n", test_02());
    printk(KERN_INFO "test_ex02: Bye...\n");
    return 0;
}
```
- Makefile:
```
obj-m += ex02.o test_ex02.o
all:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
test:
    # Add your implementation here
```
4. Implement a goal called test
5. Build the previously created kernel modules for Linux deployed with Quemu. 
Install and remove them
