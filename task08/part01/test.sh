#! /bin/env bash

declare -r TARGET=reminder

main() {
	dmesg -C
	insmod ${1}.ko
	cat /sys/class/task_08/user_id
	echo "1" > /sys/class/task_08/user_id
	echo "2 reminder" > /sys/class/task_08/message
	cat /sys/class/task_08/message
	sleep 2
	cat /sys/class/task_08/message
	cat /sys/class/task_08/message
	rmmod ${1}
	dmesg
}
main $@
