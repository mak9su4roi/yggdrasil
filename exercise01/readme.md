# Exercise I

## Description

Write bash-script that deletes files, in directories passed as arguments, such as:
- ending with `.tmp`
- beginning with `-`, `_` or `~` 

## Requirements

Directory name: `exercise01`

## Extra

1. If no arguments passed use current directory 
2. If either `-h` or `--help` passed, display usage info
3. if either `-r` or `--recursive` passed:
    - delete corresponding files in nested directories
    - delete empty directories after recursive deletion, except root of deletion 
4. if either `-y` or `--yes` passed as the first argument, assume "yes" as answer to all prompts and run non-interactively 
5. if either `-t` or `--test` passed, print filepath, do not delete

